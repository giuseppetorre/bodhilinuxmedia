## Bodhi Linux Media 5.0.0 - [download image file (.iso) here](https://zenodo.org/record/3497299#.Xah9mvco9hE).
  

Bodhi Linux Media is a free and open source distro that comes with a curated list of open source software for digital artists working with audio, video, games, graphics, animation, physical computing etc.

Scroll down for installation instructions or follow links below :  

- ###### [| How to create a Live-USB |](#how-to-create-a-live-usb) 
- ###### [| How to install on Hard Disk |](#how-to-install-it-on-your-own-computer) 
- ###### [| How to Dual Boot |](#how-to-dual-boot) 
- ###### [| Post Installtion Stuff |](#post-installtion-stuff) 
- ###### [| Support & Help |](#support)


++++++++++++++++++++++++++++++++++++++++++++++

Bodhi Linux Media is based on [Bodhi Linux](https://www.bodhilinux.com/) (Ubuntu-based OS)

++++++++++++++++++++++++++++++++++++++++++++++


![alt text](img/s11.png)

NEW ADDITIONS in Version 5.0.0:
- BLENDER 2.80
- CINELERRA : Video Editing Suite
- GODOT : 2D/3D Game Engine
- REN'PY : Visual Novel Engine
- FIREFOX : Default search engine is QWANT .

All other software as in previous version:

- ATOM : text editor for coders
- ARDUINO : IDE for Arduino Board
- PROCESSING : Java-based toolkit for coding audio visuals etc...
- PUREDATA : audio / visual coding by means of graphical object oriented programming.
- BLENDER : Alternative to 3D MAX/MAYA but also PREMIERE etc (yes Blender is an awesome video Editing tool!)
- GIMP : Alternative to Adobe Photoshop
- INKSCAPE : Alternative to Adobe Illustrator
- KRITA : software for Cartoonist
- NATRON : Alternative to Adobe After Effect
- SYNFIG STUDIO : 2D Animation
- SCRIBUS : Alternative to Adobe InDesign
- ARDOUR : Alternative to any professional DAW
- AUDACITY : Audio Editor
- SUPERCOLLIDER : Live Coding!
- MUSESCORE : Notation Software
- OBS  : Streaming and Screencasting software
- VLC : Media Player
- LIBRE OFFICE  : Documents, slides, tables etc


Bodhi Linux is a great operating system that comes with very little software installed (which is a great idea! Why installing an OS with 2 billion apps when you need 3??!).
Counterintuitevily to my above praise, I have created a version of Bodhi Linux which comes with lots of software that digital artists, researchers, students and/or enthusiast may be interested in.
The scope is to give an easy entry point to art practitioners in the world of opensource.

Should you like the system you can then decide where to go next (installing it, create your customised os etc)....
....and please consider donating (if you can, not a must) to those who developed the software you will be using for your work (not me, I have done very little, thanks. If you want to donate to the main Bodhi project instead click [here](https://www.bodhilinux.com/donate/).

Newest version is 5.0.0. **If you are looking for older version of this distro** you can download the .iso here:

- version 4.5.0 [download image file (.iso) here](https://ulir.ul.ie/bitstream/handle/10344/7162/bodhilinuxmedia-4.5.0.iso?sequence=1) --- Or you can always browse through this repository
- for version 4.5.0 login credentials are: /user:/ Bodhi-Linux-Media  /password:/ digitalart
## How to create a Live USB 

These procedure work on Linux, MAC, WIN

1) Download the Bodhilinuxmedia-5.0.0.ISO by either Forking this entire repository or simply by clicking [HERE](https://zenodo.org/record/3497299#.Xah9mvco9hE)

2) Download and install Etcher at:  https://etcher.io/

3) Follow the three steps procedure in Etcher to install the ISO onto your USB

4) Reboot you computer from USB (e.g. on Mac you hold the Alt key at startup, on other computers you press ESC or F key.. it depends, check the screen at startup)

5) login credentials are:
- user: picasso
- password: picasso

That is it. Have fun trying Bodhi Linux Media.

## How to install it on your own computer
So you love this OS and you want to thrash Win and/or Mac. Good job :)

This distro comes with Systemback. The original Bodhi linux comes with Bodhibuilder but I could not get it to work so I used Systemback.

This procedure will erase all data on your computer. If you do not want to erase all data please create a back up first.

- Boot Bodhi Linux Media via USB (see instruction above if not sure) 
- *mouse-left-click* on any empty space on the Desktop and go to *Applications --> System Tools --> Systemback*

![alt text](img/w01.png)



- Click on 'System Install'.
- Create name password etc. for your system (for example I choose "picasso" / see the image below)
- Click "Next"

 ![alt text](img/w02.png)
 

- The image below shows two disks: /sda and /sdb. The /dev/sdb disk is the USB on which Bodhi is currently running hence I do NOT want to touch it. In your system it may appear with a different name (e.g. /sdc).
- We want to instead erase partion /sda. (NOTE: this will erase all your data stored in that partition....if you have Windows on this partition then bye bye Windows)
- Click on /dev/sda and then click on the button !Delete!

 
 ![alt text](img/w03.png)


- We have now erased /sda. As you may have already noticed, I am running all this on an old computer that has only 74.5 GB of space. Our next goal is to create 3 partions out of these 74.5 Gb of space (your HD size may vary and you can scale waht follows accordingly)
- Ready? Go!
- I want to allocate  20 Gb for the /    partition  hence:
    - Select /dev/sda?  
    - input 20000 (where it says 76293 in the image below... it is othe box on the right) and hit the green arrow pointing left... NOTE: 20000 is 20/76 GB available. You can of course increase or decrease this value to anything but it is probably good to have this partition about a quarter of your overall HD space.

 ![alt text](img/w04.png)

- We have created our first partition, yes! Now we have to format it:
    - Select /dev/sda1 
    - On "Mount point:" select  / 
    - Filesystem: ext4
    - hit the green arrow pointing left

 ![alt text](img/w05.png)
 
- Select /dev/sda?  (which tells us how much space we have left: i.e. 54.97 GB in my case)
- I am now taking 40000 MiB from it (because I want to reserve  a small bit of space for my third partition - the SWAP described below). Do your maths then. If you have a 1TB HD then you can make this partiion 960000 MB or something like that. Or if you prefer just subtract a few gigabyte from the remaining space you have gotten. 
- Type your desired amount then (40000 in my case, see picture below) and hit the green arrow pointing left


 ![alt text](img/w06.png)

- We have created our second partition, yes! Now we have to format it:
    - Select /dev/sda2 
    - On "Mount point:" select  */home* 
    - Filesystem: ext4
    - hit the green arrow pointing left


 ![alt text](img/w07.png)
 
- We have to create our third and last partiton hence:
    - Select /dev/sda? No need to change the amount shown this time (it is the reminder)...
    - ...so just hit the green arrow pointing left.
    - Select /dev/sda3
    - On "Mount point:" select  SWAP 
    - Filesystem: is greyed out...that is normal
    - hit the green arrow pointing left
 
![alt text](img/w08.png)
 

- You should now have:
    -  /dev/sda1  ext4   /
    -  /dev/sda2  ext4   /home
    -  /dev/sda3  SWAP 
        -  (if *new mount point* or *filesystem* do not appear labelled/marked as in the image below, go back to each sda(1/2/3) and re-allocate SWAP / /home ext4 as appropriate and hit arrow pointing left again.)
    
    -  Tick the "Transfer user configuration and data files"   
    -  "Install  GRUB 2 bootloader:"    /dev/sda2


- Hit Next

![alt text](img/w09.png)


- Done!
- Click on START and wait for the installation to happen.
- When it is done installing, turn off the computer. remove your USB, restart your computer


![alt text](img/w10.png)


# How to Dual Boot

Procedures to create a dual boot partition vary according to the OS you use. However, no panic. You can find details online quite easily.
In any case I would recommend you install reFIind boot loader (instructions [here](http://www.rodsbooks.com/refind/)) as first thing. 
The main steps you need to follow are the following:

- Boot into your existing OS (Windows, OSX, etc.)
- Install reFIind boot loader (instructions [here](http://www.rodsbooks.com/refind/))
- Navigate to your Disk Utilities and create/add a new partition (on which you will install Bodhi Linux Media later). [Win instructions](https://www.youtube.com/watch?v=f8WzSRQ2NtY) / [Mac instructions](https://www.youtube.com/watch?v=EcRlrGpKGh8) 
- Create a USB live version of Bodhi Linux Media (my instructions [here](#how-to-create-a-live-usb))
- Reboot your system from USB
- At this point you simply need to follow the instructions for Systemback I provided in the paragraph [above](#how-to-install-it-on-your-own-computer)  BUT:
    - be careful(!) to select the correct sd(X) when following the procedure. The sd(X) should be the one you create here a few steps ago. You can recognise it easily because of the size you allocated to it during "create new partion"
    
- Reboot (you should land on the reFInd page; then select Bodhi Linux Media)
- Done
- Check the Post Installation stuff below for any doubts



# Post-Installtion Stuff

## Issue 1 - Wallpaper is not Bodhimedia Logo
- it can happen that the Wallpaper is not the Bodhi-Linux-Media one anymore . If you like the Bodhi-Linux-Media Wallpaper you can set it like this:
    - mouse left-click on any part of the desktop
    - navigate to *Settings -> Wallpaper*
    - You should see the logo-bodhimedia file
    - click on it
    - hit Apply

## Issue 2 - File Manager cannot be accessed through mouse menu
- when you *mouse-left-click* and go to *Places --> Home* the following error appears (see image)
  
![alt text](img/w11.png)
 
- SOLUTION: You need to activate this function by doing this:
    -   mouse-left-click and go to *Settings --> Settings Panel*
    -   Navigate to the tab named "Files"
    -   Click on "Places"
    -   Tick the "Use a custom file manager" and input the following text on the box below:  pcmanfm  (see image below)
    -   Hit Apply
    -   Close
    -   Done

![alt text](img/w12.png)





## Support
Bodhi Linux Media is just Bodhi Linux so for any troubles/questions etc. you can read this wiki/how to:

- http://www.bodhilinux.com/w/bodhi-linux-how-to/

or post your questions to the following forums:

- https://www.linuxquestions.org/questions/bodhi-92/
- https://www.reddit.com/r/bodhilinux/


I hope you enjoy this!
