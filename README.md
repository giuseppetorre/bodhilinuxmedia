## Bodhi Linux Media 5.1.0  

#### download .iso from Zenodo [here](https://zenodo.org/record/4553746#.YDJildbLdhE ) - - - - - or - - - - - direct download .iso link [here](https://zenodo.org/record/4553746/files/bodhimedia-5.1.0-64.iso?download=1) 

VERSION with GRAPHICAL INSTALLER!
  

Bodhi Linux Media is a free and open source distro that comes with a curated list of **open source software for digital art practitioners working with audio, video, games, graphics, animation, physical computing etc.**


- ###### [| How to create a Live-USB |](#how-to-create-a-live-usb) 
- ###### [| How to install on Hard Disk |](#how-to-install-it-on-your-own-computer) 
- ###### [| How to Dual Boot |](#how-to-dual-boot) 
- ###### [| Support & Help |](#support)


++++++++++++++++++++++++++++++++++++++++++++++

Bodhi Linux Media is based on [Bodhi Linux](https://www.bodhilinux.com/) (Ubuntu-based OS)

++++++++++++++++++++++++++++++++++++++++++++++


![alt text](img/5-1-0-desktop.jpg)

LIST OF SOFTWARE: 


- ARDUINO : IDE for Arduino Board
- PROCESSING : Java-based toolkit for coding audio visuals etc...
- PUREDATA : audio / visual coding by means of graphical object oriented programming.
- BLENDER : 3D  (Note: Blender is an awesome video Editing tool too!)
- GIMP : Alternative to Adobe Photoshop
- INKSCAPE : Alternative to Adobe Illustrator
- KRITA : software for Cartoonist
- NATRON : Alternative to Adobe After Effect
- SCRIBUS : Alternative to Adobe InDesign
- ARDOUR : Alternative to any professional DAW
- AUDACITY : Audio Editor
- SUPERCOLLIDER : Live Coding!
- MUSESCORE : Notation Software
- OBS  : Streaming and Screencasting software
- CINELERRA: video editor
- KDENLIVE: video editor
- GODOT : 2D/3D Game Engine
- REN'PY : Visual Novel Engine
- FIREFOX : Default search engine is QWANT
- VLC : Media Player
- LIBRE OFFICE  : Documents, slides, tables etc

|  |  |  |
| ------ | ------ | ------ |
| ![alt text](img/5-1-0-ardour.jpg) | ![alt text](img/5-1-0-krita.jpg) | ![alt text](img/5-1-0-blender.jpg)|





Bodhi Linux is a great operating system that comes with very little software installed (which is a great idea! Why installing an OS with 2 billion apps when you need 3??!).
Counterintuitevily to my above praise, I have created a version of Bodhi Linux which comes with lots of software that digital artists, researchers, students and/or enthusiast may be interested in.
The scope is to give an easy entry point to art practitioners in the world of opensource.

Should you like the system you can then decide where to go next (installing it, create your customised os etc)....
....**and please consider donating (if you can, not a must)** to those who developed the software you will be using for your work (not me, I have done very little, thanks. If you want to donate to the main Bodhi project instead click [here](https://www.bodhilinux.com/donate/).** To donate to developer of software check their official pages**.


# How to create a Live USB 

These procedure work on Linux, MAC, WIN

1) Get a USB key of at least 4Gb. If you find a spare one at home, backup all data in it somewhere as it will be erased. 

2) Download the Bodhilinuxmedia-5.1.0.ISO by by clicking [HERE](https://zenodo.org/record/4553746#.YDJildbLdhE)

3) Download and install Balena Etcher at:  https://www.balena.io/etcher/ 

4) Follow the three steps procedure in Etcher to install the ISO onto your USB

![alt text](img/5-1-0-steps-etcher.gif)


4) Reboot you computer from USB (e.g. on Mac you hold the Alt key at startup, on other computers you press ESC or F key.. it depends, check the screen at startup)


That is it. Have fun trying Bodhi Linux Media.

# How to install it on your own computer

This version of Bodhi Linux Media comes with a graphical installer (Ubuntu's Ubiquity).
Please note: this method will erase all data on your compurter. If you wish to run Bodhi Linux Media alongside your current operating systems please refer to section [| How to Dual Boot |](#how-to-dual-boot) 




| BOOT your LIVE USB, then:  | --------  | click on images to enlarge |
| ------ | ------ | ------ |
| ![alt text](img/5-1-0-install-01.jpg) | ![alt text](img/5-1-0-install-02.jpg) | ![alt text](img/5-1-0-install-03.jpg)|
| ![alt text](img/5-1-0-install-04.jpg) | ![alt text](img/5-1-0-install-05.jpg) | ![alt text](img/5-1-0-install-06.jpg)|
| ![alt text](img/5-1-0-install-07.jpg) | ![alt text](img/5-1-0-install-08.jpg) | ![alt text](img/5-1-0-install-09.jpg)|
| ![alt text](img/5-1-0-install-10.jpg) | ![alt text](img/5-1-0-install-11.jpg) | ![alt text](img/5-1-0-install-12.jpg)|



# How to Dual Boot

You can install Bodhi Linux Media alongside your current operating system. You can then decide each time you boot your machine which operating system to use. 
Here the steps required. In my case, I am installing Bodhi Media alongside Debian (in your case could be anything else Win, Mac, Arch etc.).   

| BOOT your LIVE USB, then:  | -------- | click on images to enlarge |
| ------ | ------ | ------ |
| ![alt text](img/5-1-0-install-01.jpg) | ![alt text](img/5-1-0-install-02.jpg) | ![alt text](img/5-1-0-install-03.jpg)|
| ![alt text](img/5-1-0-install-04.jpg) | ![alt text](img/5-1-0-install-05.jpg) | ![alt text](img/5-1-0-install-06.jpg)|
| ![alt text](img/5-1-0-install-07dual.jpg) | ![alt text](img/5-1-0-install-08dual.jpg) | ![alt text](img/5-1-0-install-09dual.jpg)|
| ![alt text](img/5-1-0-install-10dual.jpg) | ![alt text](img/5-1-0-install-11dual.jpg) | ![alt text](img/5-1-0-install-12dual.jpg)|
| ![alt text](img/5-1-0-install-13dual.jpg) | ![alt text](img/5-1-0-install-14dual.jpg) | ![.](img/5-1-0-install-none.jpg)|
.







## Support
Bodhi Linux Media is just Bodhi Linux so for any troubles/questions etc. you can read this wiki/how to:

- http://www.bodhilinux.com/w/bodhi-linux-how-to/

or post your questions to the following forums:

- https://bodhilinux.boards.net/


I hope you enjoy this!
